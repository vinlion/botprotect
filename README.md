## BotProtect
Botprotect is a software aimed to detect bot program execution on a computer or more generally a host. The software is written using C++ and uses packets captured using the [libpcap](https://www.tcpdump.org) library. The captured packets are
grouped into flows according to the IPFIX protocol presented in [RFC 5101](https://tools.ietf.org/html/rfc5101). The flows are then analysed to find evidence of bot program running and action is
the user is alarmed for proper action taking.


---

## Current Functions
Currently, the software has the following capabilities :

1.	Detect at each network card changes, the best interface to listen to.
2.	Capture the packets and retrieve key informations (Source and Destination IP Addresses, Source and Destination Port, Packet Length, Protocol, Timestamp, ID) required to form the flows
3.	Gathers the packet and group them into flows
4.	Prints the flow at end of execution

---

## What's next
The features to be included in the next version includes but are not limited to :

1.	Detecting the application protocol under each flow.
2.	Make a print of the flows with a certain fequency (maybe all 10 mns)
3.	Save the flows in an ipfix file for manual processing and logs collection
4. 	Try to detect the software initiator of the communication.

---

## Requirements
To run this program, the following are needed :

1. Libpcap/Winpcap library should be installed
2. On UNIX hosts, program should be run as root
3. gcc and g++ compilers or any similar software

---

Execution
===
Compilation
---
To compile the program, issue the below command :

```g++ -Wall -o "botprotect" "botprotect.cc" packet.cc headers.cc utils.cc flowkey.cc flow.cc --std=c++11 -lpcap `pkg-config --cflags --libs libnm gtk+-3.0` -I include -lX11 ```

Running
---
On UNIX hosts (only supported now), run :

1. Go to the install directory
2. Run sudo ./botprotect