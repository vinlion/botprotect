/*
 * botprotect.cc
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "botprotect.h"

BotProtect::BotProtect(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade) : Gtk::Window(cobject), builder(refGlade), int_Dispatcher(),
							out_Dispatcher(), in_Dispatcher(), alarm_Dispatcher(), detection_Dispatcher(), tcp_Dispatcher(), udp_Dispatcher(), ip_Dispatcher(), 
							ip6_Dispatcher(), closer()
{
	builder->get_widget("interface", int_name);
	builder->get_widget("interface_status", int_status);
	builder->get_widget("detection_notice", alarm_status);
	builder->get_widget("status", status);
	builder->get_widget("in_flows", in);
	builder->get_widget("out_flows", out);
	builder->get_widget("detected_count", detected);
	builder->get_widget("alarm_count", alrm);
	builder->get_widget("ip_count", ip);
	builder->get_widget("ip6_count", ip6);
	builder->get_widget("udp_count", udp);
	builder->get_widget("tcp_count", tcp);
	builder->get_widget("botprotect_notice", notice);
	builder->get_widget("detection_notice", alarm_status);
	
	
	// The 4 threads should be started here
	// Setting dispatchers connection
	int_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_interface));
	in_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_out_flows));
	out_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_in_flows));
	alarm_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_alarms_infos));
	detection_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_detection_infos));
	tcp_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_tcp));
	udp_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_udp));
	ip_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_ip));
	ip6_Dispatcher.connect(sigc::mem_fun(*this, &BotProtect::update_ip6));
	closer.connect(sigc::mem_fun(*this, &BotProtect::close_dialog));
	
	Glib::Thread::create(sigc::mem_fun(*this, &BotProtect::startCapture), true);
	Glib::Thread::create(sigc::mem_fun(*this, &BotProtect::listen_interfaces_events), true);
	Glib::Thread::create(sigc::mem_fun(*this, &BotProtect::checkExpiredFlows), true);
	
	strcpy(live_interface, getSuitableInterface());
	int_Dispatcher.emit();
	
}
BotProtect::BotProtect(){
	capture_pid= (int)getpid();
}
void BotProtect::close_dialog(){
	if (mdialog != NULL)
		gtk_widget_destroy((GtkWidget*)(mdialog->gobj()));
}
void BotProtect::listen_interfaces_events(){
	struct sockaddr_nl addr;
    int sock, len;
    char buffer[4096];
    struct nlmsghdr *nlh;

    if ((sock = socket(PF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) == -1) {
        perror("couldn't open NETLINK_ROUTE socket");
        exit(1);
    }

    memset(&addr, 0, sizeof(addr));
    addr.nl_family = AF_NETLINK;
    addr.nl_groups = RTMGRP_IPV4_IFADDR;

    if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        perror("couldn't bind");
        exit(1);
    }

    nlh = (struct nlmsghdr *)buffer;
    while ((len = recv(sock, nlh, 4096, 0)) > 0) {
        while ((NLMSG_OK(nlh, len)) && (nlh->nlmsg_type != NLMSG_DONE)) {
            if (nlh->nlmsg_type == RTM_NEWADDR || nlh->nlmsg_type == RTM_DELADDR) {
				check_interface = strcmp (live_interface, getSuitableInterface()) != 0;
				if (check_interface){
					std::cout << "Interface changed from [" << live_interface << "] to [" << getSuitableInterface() << "]\n";
				}
            }
            nlh = NLMSG_NEXT(nlh, len);
        }
    }
}
char* BotProtect::getSuitableInterface(){
	char errbuf[PCAP_ERRBUF_SIZE], *name;
	name = pcap_lookupdev (errbuf);
	if (name != NULL){
		return name;
	}
	else{
		std::cerr << "Error retrieving interface suitable for capture : " << errbuf << std::endl;
		return NULL;
	}
}

void BotProtect::update_interface(){
	int_name->set_text(live_interface);
	if (strcmp(live_interface, "any") == 0){
		int_status->set_from_resource("/com/gtk/botprotect/assets/images/stop.png");
		//status->set_attributes("foreground", "#ffff00000000");
		status->set_text("Capture arrêtée");
	}
	else{
		int_status->set_from_resource("/com/gtk/botprotect/assets/images/circleb.png");
		//status->set_foregroung("#0000ffff0000");
		status->set_text("Capture en cours");
	}
}
void BotProtect::update_in_flows(){
	in->set_text(std::to_string(++data_in));
}
void BotProtect::update_out_flows(){
	out->set_text(std::to_string(++data_out));
}
void BotProtect::update_alarms_infos(){
	// If registered alarm, set image to warning and change text
}
void BotProtect::update_detection_infos(){
	detected->set_text(std::to_string(processed.size()));
	FlowKey key = f_detected.back();
	int detection = a_detected.back();
	
	std::string name("");
	int pid;
	programInfo(key, name, pid);
	/* Here we will make the reverse name resolution */
	struct sockaddr_in sa, da;
	char sname[NI_MAXHOST], dname[NI_MAXHOST];
	memset(&sa, 0, sizeof sa);
	memset(&da, 0, sizeof sa);
	int family = key.getVersion() == 4 ? AF_INET : AF_INET6;
	sa.sin_family = family;
	da.sin_family = family;
	inet_pton(family, key.getSourceIPAddress()->toString().c_str(), &sa.sin_addr);
	inet_pton(family, key.getDestinationIPAddress()->toString().c_str(), &da.sin_addr);
	int sres = getnameinfo((struct sockaddr*)&sa, sizeof(sa),
						  sname, sizeof(sname),
							  NULL, 0, NI_NAMEREQD), 
		dres = getnameinfo((struct sockaddr*)&da, sizeof(da),
							  dname, sizeof(dname),
							  NULL, 0, NI_NAMEREQD);
	std::string a, source = (sres || (std::string(sname).size()) <= 5) ? "l'hôte " + key.getSourceIPAddress()->toString() : std::string(sname), 
		destination = (dres || (std::string(dname).size()) <= 5) ? "l'hôte " + key.getDestinationIPAddress()->toString() : std::string(dname);
	a = "Une conversation réseau entre &lt;b&gt; " + std::string(sname) + "&lt;/b&gt; et " + std::string(dname) + " générée à partir de \n" + name + " présente des caractéristiques d'une communication malicieuse.\nVeuillez confirmer si oui ou non la communication est normale.\n Cliquez sur Continuer pour accepter la communication et sur Annuler pour l'arrêter.";
	 //Check program details here
	Gtk::Dialog *dialog;
	builder->get_widget("detection_dialog", dialog);
	Gtk::Label *details, *full_from, *full_to, *attack_name, *proto;
	Gtk::Button *activator, *close;
	builder->get_widget("attack_detected_details", details);
	builder->get_widget("attack_details_source", full_from);
	builder->get_widget("attack_details_destination", full_to);
	builder->get_widget("attack_details_protocol", proto);
	builder->get_widget("attack_details_name", attack_name);
	builder->get_widget("attack_process_action", activator);
	builder->get_widget("close_dialog", close);
	details->set_text(a);
	full_from->set_text(key.getSourceIPAddress()->toString() + ":" + std::to_string(key.getSourcePort()));
	full_to->set_text(key.getDestinationIPAddress()->toString() + ":" + std::to_string(key.getDestinationPort()));
	proto->set_text(key.getProtocolName());
	switch (detection){
		case FLOODING_ATTACK:
			a = "Flooding";
			break;
		case FLOODING_ATTACK_DEEP:
			a = "Flooding";
			break;
		case SPOOFING_ATTACK:
			a = "Usurpation d'adresse IP (Spoofing)";
			break;
		case SCANNING_ATTACK:
			a = "Scan de ports";
			break;
		case C2_COMMUNICATION:
			a = "Commnication avec un réseau de machines zombies (bots)";
			break;
	}
	attack_name->set_text(a);
	if (pid)
		activator->signal_clicked().connect(sigc::bind<gint>(sigc::mem_fun(*this, &BotProtect::drop_program), pid));
	else if (detection == SCANNING_ATTACK || detection == SPOOFING_ATTACK)
		activator->signal_clicked().connect(sigc::bind<gpointer>(sigc::mem_fun(*this, &BotProtect::firewall_rule), &key));
	else
		activator->signal_clicked().connect(sigc::bind<gpointer>(sigc::mem_fun(*this, &BotProtect::firewall_rule_bis), &key));
	close->signal_clicked().connect(sigc::mem_fun(*this, &BotProtect::close_dialog));
	mdialog = dialog;
	dialog->run();
}
void BotProtect::update_tcp(){
	tcp->set_text(std::to_string(++tcp_data));
}
void BotProtect::update_udp(){
	udp->set_text(std::to_string(++udp_data));
}
void BotProtect::update_ip(){
	ip->set_text(std::to_string(++ip_data));
}
void BotProtect::update_ip6(){
	ip6->set_text(std::to_string(++ip6_data));
}
void BotProtect::startCapture(){
	while (1) {
		if (live_interface != NULL){
			strcpy(live_interface, getSuitableInterface());
			int_Dispatcher.emit();
			if (strcmp(live_interface, "any") != 0){
				getInterfaceIPAddress();
				std::cerr << "Listening on << " << live_interface << " >> using process " << capture_pid << std::endl;
				char errbuf[PCAP_ERRBUF_SIZE];
				struct pcap_pkthdr *header;
				const u_char *pkt_data;
				char filter_exp[] = "not icmp";
				struct bpf_program filter;
				bpf_u_int32 subnet_mask, ip;
				int res;
				if (pcap_lookupnet(live_interface, &ip, &subnet_mask, errbuf) == -1) {
					std::cerr << "Could not get information for device:" << live_interface << std::endl;
					ip = 0;
					subnet_mask = 0;
				}
				handle = pcap_open_live(live_interface, BUFSIZ, USE_PROMISCUOUS, PACKET_TIMEOUT, errbuf);
				if (! handle){
					std::cerr << errbuf << std::endl;
					continue;
				}
				if (pcap_compile(handle, &filter, filter_exp, 0, ip) == -1) {
					std::cerr << "Bad filter - " << pcap_geterr(handle) << std::endl;
					exit(2);
				}
				if (pcap_setfilter(handle, &filter) == -1) {
					std::cerr << "Error setting filter - - " << pcap_geterr(handle) << std::endl;
					exit(2);
				}
				while((res = pcap_next_ex( handle, &header, &pkt_data)) >= 0 && ! check_interface)
				{
					if(res == PACKET_TIMEOUT)
					// Timeout elapsed 
					continue;
					
					//Form packet object and process the flows.
					if ((*(pkt_data + 14) >> 4) & 4 ) // 4 or 4+2=6 or 4+2+1(should not happen)
					{
						Packet p (header, pkt_data);
						processPacket(p);	
					}					
				}
				if(res == -1)
				{
					std::cerr << "Error reading the packets: " << pcap_geterr(handle) << std::endl;
				}
				if (check_interface){
					//Stop the loop and set the flag to false
					check_interface = false;
					std::cout << "Stopping capture on " << live_interface << " now\n";
					pcap_breakloop(handle);
					continue;
				}
			}
		}
	}
	
}
void BotProtect::getInterfaceIPAddress(){
	
	// Mettre comme attribut dans la classe et mettre à jour à chaque changement des NICs //
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *alldevs;
	int status = pcap_findalldevs(&alldevs, errbuf);
	if(status != 0)
	{
		printf("%s\n", errbuf);
	}
	std::vector<std::string> ips;
	for(pcap_if_t *d=alldevs; d!=NULL; d=d->next)
	{
		if (strcmp(d->name, live_interface) != 0)
			continue;
		for(pcap_addr_t *a=d->addresses; a!=NULL; a=a->next)
		{
			std::string res;
			if(a->addr->sa_family == AF_INET)
			{
				res = inet_ntoa(((struct sockaddr_in*)a->addr)->sin_addr);
				
			}
			else if(a->addr->sa_family == AF_INET6)
			{
				char buffer[INET6_ADDRSTRLEN];
				struct sockaddr_in6 *in6 = (struct sockaddr_in6*) a->addr;
				res = inet_ntop(a->addr->sa_family, &in6->sin6_addr, buffer, sizeof(buffer));
			}
			interfaceAddresses.insert(res);
		}
		break;
	}
	pcap_freealldevs(alldevs);
}

void BotProtect::processPacket (Packet& packet){
	// Form the flowkey of the packet
	FlowKey key (packet);
	int ind=-1;
	for (size_t i : opened_flows){
		++ind;
		if (i < flows.size()){  // Avoid accessing an invalid data that will break the program
			if (flows[i].getKey() != key)
				continue;
			if (flows[i].willExpire(packet.getTime())){
				// Remove index from the table
				opened_flows.erase (opened_flows.begin() + ind);
				// Suspicious check
				int check = flowIsSuspect(flows[i]);
				if (check)
					askUserAction(key, check);
				break;
			}
			 //The flow is the best one to deal with
			flows[i] += packet;
			return;
		}
	}
	// Should make a new entry for it
	opened_flows.push_back(flows.size());  // Getting the size now avoids having to extract one after pushing back flows
	flows.push_back(Flow(packet));
	if (key.getProtocol() == TCP)
		tcp_Dispatcher.emit();
	else
		udp_Dispatcher.emit();
	if (key.getVersion() == 4)
		ip_Dispatcher.emit();
	else
		ip6_Dispatcher.emit();
	if (interfaceAddresses.find(key.getSourceIPAddress()->toString()) != interfaceAddresses.end()) {
		++summary[key.getDestinationIPAddress()->toString()][key.getDestinationPort()];
		++temp_data[key.getDestinationIPAddress()->toString()][key.getDestinationPort()];
		out_Dispatcher.emit();
		if ( summary[key.getDestinationIPAddress()->toString()].size() >= DESTINATION_PORT_THRESOLD ){
			// This is a scanning attack
			askUserAction(key, SCANNING_ATTACK);
		}
		else if (summary[key.getDestinationIPAddress()->toString()][key.getDestinationPort()] >= NUMBER_OF_FLOW_THRESOLD){
			// This is a flooding attack
			askUserAction(key, FLOODING_ATTACK_DEEP);
		}
	}
	else
		in_Dispatcher.emit();
}
void BotProtect::askUserAction(FlowKey key, int detection){
	Attack a;
	f_detected.push_back(key);
	a_detected.push_back(detection);
	if (detection == SCANNING_ATTACK || detection == SPOOFING_ATTACK)
		a = Attack(key.getSourceIPAddress(), 0, detection);
	else if (interfaceAddresses.find(key.getSourceIPAddress()->toString()) != interfaceAddresses.end())
		a = Attack(key.getDestinationIPAddress(), key.getDestinationPort(), detection);
	else
		a = Attack(key.getSourceIPAddress(), key.getSourcePort(), detection);
	if (std::find(processed.begin(), processed.end(), a) == processed.end()){
		processed.push_back(a);
		detection_Dispatcher.emit();
	}
}

void BotProtect::programInfo(FlowKey& key, std::string& name, int& pid){
	#ifdef _WIN32
	
	#else
	redi::ipstream is("netstat --protocol=inet,inet6 -np | awk 'toupper($1) ~ /" + key.getProtocolName() + "/ && $4 == \"" + 
				key.getSourceIPAddress()->toString() + ":" + std::to_string(key.getSourcePort()) + "\" && $5 == \"" + 
				key.getDestinationIPAddress()->toString() + ":" + std::to_string(key.getDestinationPort()) + "\" {print $7}'");
    std::string line;
    while (is >> line)
    {
        // Nothing to do
    }
    // We got the information in this format PID/Program Name
    size_t ind = line.find('/');
    try { // Try block for case where the data could not be found
		pid = stoi(line.substr(0, ind));
		name = line.substr(ind+1);
	}
	catch (std::invalid_argument& e){
		std::cerr << "An exception occurred while getting program info from flow."<< '\n';
		pid = 0;
		name = "[PROGRAMME INCONNU]";
	}
	#endif
}
int BotProtect::flowIsSuspect(Flow& flow){
	int res = flowIsAttack(flow);
	if (res){
		std::cout << "Attack detected (" << res << ") -> " << flow << std::endl;;
		return res;
	}
	std::vector<Flow> flows = isC2Communication();
	if (flows.size()){
		// We will take the first flow in the list
		std::cout << "C2 Communication detected : " << flows.back() << std::endl;
		return C2_COMMUNICATION;
	}
	return 0;
}
/**
 * Checks if a flow represents a flooding from the host running the program.
**/ 
int BotProtect::flowIsAttack(Flow& flow){
	if (interfaceAddresses.size() && interfaceAddresses.find(flow.getKey().getSourceIPAddress()->toString()) == interfaceAddresses.end()){
		// Host is not the originator of the flow. Check then if it is the destination
		if (interfaceAddresses.find(flow.getKey().getDestinationIPAddress()->toString()) == interfaceAddresses.end()){
			return SPOOFING_ATTACK;
		}
	}
	else{
		//Host is the originator. Check for flooding
		switch (flow.getKey().getProtocol()){
			case TCP:
				if (flow.getPacketCount() >= TCP_FLOW_PACKET_COUNT_SUSPICIOUS){
					auto reverse = std::find(flows.begin(), flows.end(), -flow);
					if (reverse == flows.end() || reverse->getPacketCount() < 0.25 * flow.getPacketCount()){
						// The reverse flow doesn't exist or 25% of the packets have not been replied
						return FLOODING_ATTACK;
					}
				}
				break;
			case UDP:
				if (flow.getPacketCount() > FLOODING_PACKET_COUNT_THRESOLD || flow.averagePacketSize() > UDP_FLOW_AVERAGE_THRESOLD) {
					// Number of packets is high or packets are too big for UDP
					return FLOODING_ATTACK;
				}
				break;
		}
	}
	return 0;
}
/**
 * For all currently opened flows, check the ones that have expired and process them.
 * 
*/ 
void BotProtect::checkExpiredFlows(){
	while (1) {
		std::this_thread::sleep_for(std::chrono::seconds(TIMEOUT));  // Value defined in flow.h
		summary = temp_data;
		// Copy the temporary data into the live data to just consider packets not too older
		temp_data.erase(temp_data.begin(), temp_data.end());
		// Empty the second map
		int ind = 0;
		for (int i : opened_flows){
			timeval t;
			gettimeofday (&t, NULL);
			if (flows[i].willExpire(t)) {
				// Remove index from the table
				opened_flows.erase (opened_flows.begin() + ind);
				// We've deleted so indexes should be shifted by 1
				--ind;
				// Suspicious check
				//int check = flowIsAttack(flows[i]);
				int check=1;
				if (check)
					askUserAction(flows[i].getKey(), check);
			}
			++ind;
		}
		// Check C2 Communication lastly
		std::vector<Flow> flows = isC2Communication();
		if (flows.size())
			askUserAction(flows.back().getKey(), C2_COMMUNICATION);
	}
}
void BotProtect::printResult(std::vector<Flow> flows, std::string step){
	std::cout << step << flows.size() << std::endl;
	for (auto flow : flows){
		std::cout << flow << std::endl;
	}
}
/**
 * Checks if a flow has some characteristics that denotes a communication between a bot and 
 * the C&C server.
**/ 
std::vector<Flow> BotProtect::isC2Communication(){
	int step0=flows.size(), step1=0, step2=0, step3=0, step4=0;
	std::vector<Flow> t_flows;
	for (Flow flow : flows){
		if (flow.getKey().getProtocol() == TCP){
			++step1;
			if (flow.getPacketCount() < 10){
				// Flow is not is huge file transfert
				++step2;
				if (flow.averagePacketSize() <= 300 ){
					// Packets in flow are not large
					++step3;
					if (flow.getPacketCount() >= 5 && flow.duree() < 20.0){
						t_flows.push_back(flow);
					}
				}
			}
		}
	}
	step4 = t_flows.size();
	for (int i=0; t_flows.begin()+i != t_flows.end(); ++i){ 
		std::vector<Flow>::iterator reverse = std::find(t_flows.begin() + i, t_flows.end(), -t_flows[i]);
		if (reverse != t_flows.end()){
			// Make a swap to get both values at the same place
			std::iter_swap(reverse, t_flows.begin()+i+1);
			if (fabs(t_flows[i].getPacketCount() - t_flows[i+1].getPacketCount()) <= 1 ){
				// It's okay so i should be incremented
				++i;
				continue;
			}
			else
				t_flows.erase(t_flows.begin() +i+1);
		}
		// The flow should be deleted
		t_flows.erase(t_flows.begin() + i);
		--i;
	}
	std::cout << "Detection Flow\t" << step0 << " => " << step1 << " => " << step2 << " => " << step3 << " => " << step4 << " => " << t_flows.size() << std::endl; 
	return t_flows;
}

void BotProtect::drop_program(gint pid){
	std::string cmd ("kill -9 " + std::to_string(pid));  // SIGKILL in POSIX
	system(cmd.c_str());
	close_dialog();
}
void BotProtect::firewall_rule(gpointer data){
	FlowKey key = *((FlowKey*)data);
	std::string cmd;
	if (BotProtect::interfaceAddresses.find(key.getSourceIPAddress()->toString()) != BotProtect::interfaceAddresses.end())
		cmd = "iptables -I OUTPUT -p " + key.getProtocolName() + " -d " + key.getDestinationIPAddress()->toString() + " --dport " + std::to_string(key.getDestinationPort()) + " -j DROP";
	else
		cmd = "iptables -I INPUT -p " + key.getProtocolName() + " -s " + key.getSourceIPAddress()->toString() + " --sport " + std::to_string(key.getSourcePort()) + " -j DROP";
	system(cmd.c_str());
	close_dialog();
}
void BotProtect::firewall_rule_bis(gpointer data){
	FlowKey key = *((FlowKey*)data);
	std::string cmd;
	if (BotProtect::interfaceAddresses.find(key.getSourceIPAddress()->toString()) != BotProtect::interfaceAddresses.end())
		// Here it is scanning attack
		cmd = "iptables -I OUTPUT -p " + key.getProtocolName() + " -d " + key.getDestinationIPAddress()->toString() + " -j DROP";
	else
		// Here it is spoofing attack so we block all the packets from that IP
		cmd = "iptables -I OUTPUT -p " + key.getProtocolName() + " -s " + key.getSourceIPAddress()->toString() + " -j DROP";
	system(cmd.c_str());
	close_dialog();
}
int main(int    argc,
      char **argv){
    
	/**
	 * Start the interfaces listener and initial capture concurrently
	 * so each change in NICs should automatically raise a change on
	 * the interface being listened to.
	 * 
	**/
	if (Glib::thread_supported())
		Glib::thread_init();
	else
	{
		cerr << "Threads aren't supported!" << endl;
		exit(1);
	}
	BotProtect *window;
	auto app = Gtk::Application::create("com.builder.botprotect");
	auto builder = Gtk::Builder::create_from_resource("/com/gtk/botprotect/assets/botprotect.ui");
	builder->get_widget_derived("botprotect_home", window);
	return app->run(*window);
	
}
