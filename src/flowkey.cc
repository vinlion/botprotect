/*
 * flowkey.cc
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "flowkey.h"

FlowKey::FlowKey(){
	}
FlowKey::FlowKey (IPAddress* src, IPAddress* dst, u_short sport, u_short dport, u_char proto){
	this->src = src;
	this->dst = dst;
	this->sport = sport;
	this->dport = dport;
	this->protocol = proto;
}

FlowKey::FlowKey(Packet& packet){
	this->src = &(packet.getPacketHeader()->getSourceIPAddress());
	this->dst = &(packet.getPacketHeader()->getDestinationIPAddress());
	this->sport = packet.getSourcePort();
	this->dport = packet.getDestinationPort();
	this->protocol = packet.getPacketHeader()->getProtocol();
	this->version = packet.getPacketHeader()->getVersion();
}
FlowKey::FlowKey (FlowKey const& key){
	src = key.getSourceIPAddress();
	dst = key.getDestinationIPAddress();
	sport = key.getSourcePort();
	dport = key.getDestinationPort();
	protocol =  key.getProtocol();
	version = key.getVersion();
}

FlowKey FlowKey::operator -(){
	return FlowKey (dst, src, dport, sport, protocol);
}
IPAddress* FlowKey::getSourceIPAddress () const{
	return src;
}
IPAddress* FlowKey::getDestinationIPAddress () const{
	return dst;
}
u_short FlowKey::getSourcePort() const{
	return sport;
}
u_short FlowKey::getDestinationPort() const{
	return dport;
}
u_char FlowKey::getProtocol() const{
	return protocol;
}
u_char FlowKey::getVersion() const{
	return version;
}
std::string FlowKey::getProtocolName() const{
	switch (protocol) {
		case 6:
			return "TCP";
		case 17:
			return "UDP";
		default:
			return std::to_string(protocol);
	}
}
bool FlowKey::operator== (FlowKey const& flowkey){
	return protocol==flowkey.getProtocol() && sport==flowkey.getSourcePort() && dport==flowkey.getDestinationPort() && *src==*(flowkey.getSourceIPAddress()) && *dst==*(flowkey.getDestinationIPAddress());
}
bool FlowKey::operator != (FlowKey const& flowkey){
	return ! (*this == flowkey);
}
ostream& operator << (ostream& flux, FlowKey const& flow){
	flux << *(flow.getSourceIPAddress()) <<  ":" << flow.getSourcePort() << " ==> " << *(flow.getDestinationIPAddress()) << ":" << flow.getDestinationPort() << "  (" << flow.getProtocolName() << ")";
	return flux;
}
