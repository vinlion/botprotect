/*
 * packet.cc
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include "packet.h"

Packet::Packet (struct pcap_pkthdr *header, const u_char *pkt_data){
    ip_header *ih;
    u_int ip_len;

	time = header->ts;
    data = (char *) pkt_data;
    
    /* retireve the position of the ip header */
    ih = (ip_header *) (pkt_data +
        14); //length of ethernet header
        
    u_char version = *(pkt_data + 14) >> 4;
    switch (version){
		case 4:
			pheader = new IPv4PacketHeader (pkt_data, header);
			break;
		case 6:
			pheader = new IPv6PacketHeader (pkt_data, header);
			break;
	}
	ip_len = (*(pkt_data + 14) & 0xf) * 4;
    
    //Convert the headers according to the protocol  and retrieve source and destination ports
    if (pheader->getProtocol() == TCP){
		tcp_header *th;
		th = (tcp_header *) ((u_char*)ih + ip_len);
		sport = ntohs( th->sport );
		dport = ntohs( th->dport );
		flags = th->flags;
	}
	else {
		udp_header *uh;
		uh = (udp_header *) ((u_char*)ih + ip_len);
		sport = ntohs( uh->sport );
		dport = ntohs( uh->dport );
		flags = 0;
	}
    
}
Packet::~Packet(){
	
}

u_short Packet::getSourcePort() const{
	return sport;
} 
u_short Packet::getDestinationPort() const{
	return dport;
}
u_short Packet::getFlags() const {
	return flags;
}

IPPacketHeader* Packet::getPacketHeader(){
	return pheader;
}
timeval Packet::getTime() const{
	return time;
}

std::string Packet::getData() const{
	return data;
}

std::ostream& operator << (std::ostream& flux, Packet& packet){
	flux << packet.getTime() << " " << packet.getPacketHeader()->getSourceIPAddress() << ":" << packet.getSourcePort() << " ==> " 
		<< packet.getPacketHeader()->getDestinationIPAddress() << ":" << packet.getDestinationPort()	<< " [" << packet.getPacketHeader()->getProtocolName() 
		<< "]  (" << packet.getPacketHeader()->getLength() << " bytes)";
	return flux;
}
