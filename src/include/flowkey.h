/*
 * flowkey.h
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * Last Modified on 29.05.2018
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <cstdio>
#include <iostream>
#include "packet.h"

using namespace std;

class FlowKey{
	private:
		IPAddress *src, *dst;
		u_short sport, dport;
		u_char protocol, version;
	public:
		FlowKey ();
		FlowKey (IPAddress*, IPAddress*, u_short, u_short, u_char);
		FlowKey (Packet&);
		FlowKey (FlowKey const&); // Constructor of copy
		// Get the inverse flowkey (i.e the second flowkey to make a bidirectionnal flow of it)
		FlowKey operator - ();
		bool operator == (FlowKey const&);
		bool operator != (FlowKey const&);
		//Getters
		IPAddress* getSourceIPAddress () const;
		IPAddress* getDestinationIPAddress () const;
		u_short getSourcePort() const;
		u_short getDestinationPort() const;
		u_char getProtocol() const;
		u_char getVersion() const;
		std::string getProtocolName() const;
};

ostream& operator << (ostream &, FlowKey const&);
