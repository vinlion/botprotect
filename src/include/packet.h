/*
 * packet.h
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include <cstring>
#include <iostream>
#include <ctime>
#include <pcap/pcap.h>
#include <arpa/inet.h>

#include "utils.h"
#include "headers.h"

#define TCP 6
#define UDP 17
#define ICMP 1

class Packet {
private:
	u_short sport, dport, flags; // TCP Flags for the packet, set to 0 for UDP
	IPPacketHeader *pheader;
	timeval time;
	std::string data;
	
	//Declare prototypes for functions here
public:
	Packet (struct pcap_pkthdr*, const u_char*);
	~Packet();
	
	/*
	 * Getters and setters.
	 * 
	*/
	u_short getSourcePort() const;
	u_short getDestinationPort() const;
	u_short getFlags() const;
	IPPacketHeader* getPacketHeader();
	timeval getTime() const;
	std::string getData() const;
};

std::ostream& operator << (std::ostream&, Packet&);
