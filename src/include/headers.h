/*
 * headers.h
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include "pcap.h"

#define PACKET_HEADERS

/*
 * Base class for IP Addresses
 * 
*/
class IPAddress {
	// Let us make compulsory a method to String for IP Addresses that will be used to print using ostream
	public:
		virtual std::string toString () const = 0;
		bool operator == (IPAddress const&);
		//bool operator == (IPAddress*);
		bool operator == (std::string const&);
};


class IPv4Address : public IPAddress {
	// An IP v4 Address has a set of 4 bytes (4*8 bits)
	private:
		u_char bytes [4];
	public:
		IPv4Address (u_char=0, u_char=0, u_char=0, u_char=0);
		std::string toString () const;
};

class IPv6Address : public IPAddress {
	// An IP v6 address is a set of 8*16 bits
	private:
		u_short bytes [8];
		short zero_start_index=0, zeroes_length=0;
		// Utility function to get the longest serie of zeroes in the IP Address to properly format it
		void checkLongestSeq();
	public:
		IPv6Address (u_short=0, u_short=0, u_short=0, u_short=0, u_short=0, u_short=0, u_short=0, u_short=0 );
		std::string toString () const;
};

// Base class for IPPacketHeader
class IPPacketHeader {
	protected:
		u_char version, protocol;
		u_short length;
	public:
		// Default to IPv4/TCP
		IPPacketHeader(u_char=4, u_char=6);
		u_char getVersion() const;
		u_char getProtocol() const;
		u_short getLength() const;
		std::string getProtocolName() const;
		virtual IPAddress& getSourceIPAddress() = 0;
		virtual IPAddress& getDestinationIPAddress() = 0;
};


// IPv4PacketHeader class
/*
 *  0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |Version|  IHL  |Type of Service|          Total Length         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |         Identification        |Flags|      Fragment Offset    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Time to Live |    Protocol   |         Header Checksum       |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                       Source Address                          |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Destination Address                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Options                    |    Padding    |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   * 
  */
class IPv4PacketHeader : public IPPacketHeader {
	private:
		u_char ihl, tos, flags, offset, ttl, padding; // IHL, Type of Service, Flags, Fragment Offset, Time to Live, Padding
		u_short identification, crc; // Total Length, Identification, Checksum
		u_int options; // Options
		IPv4Address source, destination; // Evident...
	public:
		IPv4PacketHeader (const u_char*, struct pcap_pkthdr*);
		// Definition of superclass functions to get instanciable
		IPAddress& getSourceIPAddress();
		IPAddress& getDestinationIPAddress();
		u_char getIHL() const;
		u_char getTypeOfService() const;
		u_char getFlags() const;
		u_char getOffset() const;
		u_char getTimeToLive() const;
		u_char getPadding() const;
		u_short getIdentification() const;
		u_short getChecksum() const;
		u_int getOptions() const;
		
};
  
/*
 * 
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |Version| Traffic Class |           Flow Label                  |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |         Payload Length        |  Next Header  |   Hop Limit   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   +                                                               +
   |                                                               |
   +                         Source Address                        +
   |                                                               |
   +                                                               +
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                                                               |
   +                                                               +
   |                                                               |
   +                      Destination Address                      +
   |                                                               |
   +                                                               +
   |                                                               |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
   * 
   * 
*/

class IPv6PacketHeader : public IPPacketHeader {
	private:
		u_char traffic_class, hop_limit; // Traffic Class, Hop Limit
		u_int label; // Label
		IPv6Address source, destination; // Evident...
	public:
		IPv6PacketHeader (const u_char*, struct pcap_pkthdr*);
		// Definition of superclass functions to get instanciable
		IPAddress& getSourceIPAddress();
		IPAddress& getDestinationIPAddress();
		u_char getTrafficClass() const;
		u_char getHopLimit() const;
		u_int getLabel() const;
};


std::ostream& operator << (std::ostream&, IPAddress const&);


/* 4 bytes IP address */
typedef struct ip_address{
    u_char byte1;
    u_char byte2;
    u_char byte3;
    u_char byte4;
}ip_address;

//IP v6 address format
typedef struct ip6_address{
    u_short comp1;
    u_short comp2;
    u_short comp3;
    u_short comp4;
    u_short comp5;
    u_short comp6;
    u_short comp7;
    u_short comp8;
    
}ip6_address;

/* IPv4 header */
typedef struct ip_header{
    u_char  ver_ihl;        // Version (4 bits) + Internet header length (4 bits)
    u_char  tos;            // Type of service 
    u_short tlen;           // Total length 
    u_short identification; // Identification
    u_short flags_fo;       // Flags (3 bits) + Fragment offset (13 bits)
    u_char  ttl;            // Time to live
    u_char  proto;          // Protocol
    u_short crc;            // Header checksum
    ip_address  saddr;      // Source address
    ip_address  daddr;      // Destination address
    u_int   op_pad;         // Option + Padding
}ip_header;

/* IPv6 header */
typedef struct ip6_header{
	u_int ver_class_label; 	// Version(4 bits) + Traffic Class (8 bits) + Label (20 bits)
	u_short plen;			// Payload Length
	u_char proto, hop_limit;			// Next Header, Hop Limit
	ip6_address  saddr;      // Source address
    ip6_address  daddr;      // Destination address
}ip6_header;

/* UDP header*/
typedef struct udp_header{
    u_short sport;          // Source port
    u_short dport;          // Destination port
    u_short len;            // Datagram length
    u_short crc;            // Checksum
}udp_header;

/* TCP header */
typedef struct tcp_header {
    u_short sport;               /* source port */
    u_short dport;               /* destination port */
    u_int seq;                 /* sequence number */
    u_int ack;                 /* acknowledgement number */
    u_char  th_offx2;               /* data offset, rsvd */
    u_char  flags;				/* TCP flags */
    u_short window;                 /* window */
    u_short crc;                 /* checksum */
    u_short urgent;                 /* urgent pointer */
}tcp_header;
