/*
 * flow.h
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <string>
#include <vector>
#include <pcap/pcap.h>

#include "utils.h"
#include "flowkey.h"


#define TIMEOUT 5 // 10 minutes is the defaut timeout for packet expiration

class Flow {
	private:
		FlowKey key;
		int length, packet_count; // Total legnth of the packets of the flow
		std::string protocol; // Application protocol used in the flow (HTTP, FTP, SMTP, IRC, etc.)
		u_char flags_summary; // A summary of all TCP flags encountered in the flow conversation
		timeval start, end;
	public:
		Flow(Packet&);
		Flow(FlowKey);
		bool hasExpired();
		bool willExpire (timeval);
		Flow& operator += (Packet&);
		bool operator == (Flow const&);
		Flow operator - ();
		FlowKey& getKey();
		FlowKey getKey() const;
		int getLength() const;
		int getPacketCount() const;
		std::string getProtocol();
		timeval getStart() const;
		timeval getEnd() const;
		float duree() const;
		float bandwidth() const;
		bool isRSTFlow() const;
		bool isSYNFlow() const;
		bool fromSameSourceAs(Flow&);
		bool toSameDestinationAs(Flow&);
		int averagePacketSize();
};

ostream& operator << (ostream&, Flow&);
