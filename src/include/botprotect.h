/*
 * botprotect.h
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <cstring>
#include <cmath>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <thread>
#include <iostream>
#include <algorithm>
#include <chrono>
#include "pstream.h"
#include <stdexcept>
#include <netdb.h>

#include <pcap/pcap.h>
#include <gtkmm/application.h>
#include <gtkmm/main.h>
#include <gtkmm/window.h>
#include <gtkmm/dialog.h>
#include <gtkmm/label.h>
#include <gtkmm/builder.h>
#include <gtkmm/image.h>
#include <glibmm/dispatcher.h>
#include <glibmm/thread.h>
#include <glib/gtypes.h>

#include <X11/Xlib.h> 

#ifndef _WIN32
	#include <gio/gio.h>
	#include <nm-dbus-interface.h>
	#include <netinet/in.h>
	#include <linux/netlink.h>
	#include <linux/rtnetlink.h>
	#include <net/if.h>

#endif /* _WIN32 */ 

#include "flow.h"

#define INTERFACE_NAME_MAX_LENGTH 32
#define USE_PROMISCUOUS 0
#define PACKET_TIMEOUT 1000
#define FLOODING_PACKET_COUNT_THRESOLD 64
#define FLOODING_FLOW_SIZE 100000
#define NUMBER_OF_FLOW_THRESOLD 1000
#define AVERAGE_FLOW_SIZE_THRESOLD 1000
#define DESTINATION_IP_THRESOLD 25
#define DESTINATION_PORT_THRESOLD 25
#define NUMBER_OF_PACKET_SUMMATION_THRESOLD 1000
#define NUMBER_OF_PACKET_PER_FLOW 50
#define FLOW_SIZE_SUMMATION_THRESOLD 1000000
#define UDP_FLOW_AVERAGE_THRESOLD 600 
#define TCP_FLOW_PACKET_COUNT_SUSPICIOUS 4  //Number of packets starting from which the reverse flow should be looked for
#define UDP_FLOW_PACKET_COUNT_SUSPICIOUS 2

#define SPOOFING_ATTACK 1
#define FLOODING_ATTACK 2
#define FLOODING_ATTACK_DEEP 3
#define SCANNING_ATTACK 4
#define C2_COMMUNICATION 5

class Attack{
	private:
		IPAddress* target_ip;
		u_char target_port;
		int attack;
	public:
		Attack(){
			target_ip = new IPv4Address(0,0,0,0);
			target_port = 0;
			attack = 0;
		}
		Attack (IPAddress* ip, u_char port, int id){
			target_ip = ip;
			target_port = port;
			attack = id;
		}
		IPAddress* getIPAddress () const{
			return target_ip;
		}
		u_char getPort() const {
			return target_port;
		}
		int getAttack () const{
			return attack;
		}
		bool operator == (Attack const& other){
			return *target_ip == *(other.getIPAddress()) && target_port == other.getPort() && attack == other.getAttack();
		}
};

class BotProtect : public Gtk::Window{
	private:
		std::vector<Flow> flows;
		std::vector<FlowKey> f_detected;
		std::vector<int> a_detected;
		unordered_map<std::string, unordered_map<u_short, int>> summary, temp_data;
		unordered_set<std::string> interfaceAddresses;
		std::vector<Attack> processed;
		std::vector<size_t> opened_flows; // Index of flows that has not expired yet
		pcap_t *handle;
		int capture_pid, data_in=0, data_out=0, tcp_data=0, udp_data=0, ip_data=0, ip6_data=0, detection_data=0, alarm_data=0;
		bool check_interface;
		char live_interface [INTERFACE_NAME_MAX_LENGTH];
		Glib::RefPtr<Gtk::Builder> builder;
		Glib::Dispatcher int_Dispatcher, out_Dispatcher, in_Dispatcher, alarm_Dispatcher, detection_Dispatcher, tcp_Dispatcher, udp_Dispatcher, ip_Dispatcher, ip6_Dispatcher, closer;
		Gtk::Label *out, *in, *detected, *alrm, *ip, *ip6, *tcp, *udp, *status, *int_name, *notice;
		Gtk::Image *int_status, *alarm_status;
		Gtk::Dialog* mdialog;
		
	public:
		BotProtect();
		BotProtect(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& refGlade);
		char* getSuitableInterface();
		void startCapture();
		void listen_interfaces_events ();
		void sig_close (int);
		void processPacket (Packet&);
		void printResult(std::vector<Flow>, std::string);
		void getInterfaceIPAddress();
		int flowIsAttack(Flow&);
		int flowIsSuspect(Flow&);
		std::vector<Flow> isC2Communication();
		std::vector<Flow> flowFromSameSource(Flow&);
		std::vector<Flow> TCPFlows();
		
		// GUI utilities methods
		void askUserAction(FlowKey, int);
		void programInfo(FlowKey&, std::string&, int&);
		void checkExpiredFlows();
		virtual void drop_program(gint);
		void firewall_rule(gpointer);
		void firewall_rule_bis(gpointer);
		void show_about();
		void show_detection_details();
		
		// Dispatchers utilities
		
		void update_interface();
		void update_in_flows();
		void update_out_flows();
		void update_alarms_infos();
		void update_detection_infos();
		void update_tcp();
		void update_udp();
		void update_ip();
		void update_ip6();
		void close_dialog();
};
