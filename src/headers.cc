/*
 * headers.cc
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "headers.h"

// Ostream utility function for IP Addresses
std::ostream& operator << (std::ostream& flux, IPAddress const& ip){
	flux << ip.toString();
	return flux;
}

// IPAddress
bool IPAddress::operator == (IPAddress const& other){
	return toString() == other.toString();
}
/*bool IPAddress::operator == (IPAddress* other){
	return toString() == other->toString();
}*/
bool IPAddress::operator == (std::string const& string){
	return toString() == string;
}


// IPv4Address

IPv4Address::IPv4Address (u_char byte1, u_char byte2, u_char byte3, u_char byte4){
	bytes[0] = byte1; bytes[1] = byte2; bytes[2] = byte3; bytes[3] = byte4;
}
std::string IPv4Address::toString () const {
	return std::to_string(bytes[0]) + "." + std::to_string(bytes[1]) + "." +std::to_string(bytes[2]) + "." +std::to_string(bytes[3]);
}


// IPv6 Address
IPv6Address::IPv6Address (u_short c1, u_short c2, u_short c3, u_short c4, u_short c5, u_short c6, u_short c7, u_short c8 ){
	bytes[0] = c1; bytes[1] = c2; bytes[2] = c3; bytes[3] = c4;bytes[4] = c5; bytes[5] = c6; bytes[6] = c7; bytes[7] = c8;
	for (int i=0; i<8; ++i){
		u_char p1 = bytes[i] >> 8, p2=bytes[i] & 0xff;
		bytes[i] = (((u_short)(p2)) << 8) + p1;
	}
	checkLongestSeq();
}
std::string IPv6Address::toString () const {
	std::string ans = "";
	int i = 0;
	while (i<8){
		if (zeroes_length > 0 && i==zero_start_index){
			ans += (i==0) ? "::" : ":";
			i += zeroes_length;
			continue;
		}
		std::stringstream oss;
		oss << std::setw(4) << std::setfill('0') << std::hex << bytes[i];
		ans += oss.str();
		++i;
		if (i<8)
			ans += ":";
	}
	return ans;
}

void IPv6Address::checkLongestSeq()
{
    // Variables to keep track of maximum length and 
    // starting point of maximum length. And same 
    // for current length.
    int currLen = 0, currIdx = 0;
 
    for (int k = 0; k < 8; k++) {
        if (bytes[k] == 0) {
            currLen++;
 
            // New sequence, store
            // beginning index.
            if (currLen == 1) 
                currIdx = k;            
        }
        if (bytes[k] != 0 || k==7) {
            if (currLen > zeroes_length) {
				zeroes_length = currLen;
                zero_start_index = currIdx;
            }
            currLen  = 0;
        }
    }
    return;
}


// IPPacketHeader
IPPacketHeader::IPPacketHeader(u_char version, u_char protocol){
	this->version = version;
	this->protocol = protocol;
}
u_char IPPacketHeader::getVersion() const{
	return version;
}
u_char IPPacketHeader::getProtocol() const{
	return protocol;
}
u_short IPPacketHeader::getLength() const{
	return length;
}
std::string IPPacketHeader::getProtocolName() const{
	switch (protocol) {
		case 6:
			return "TCP";
		case 17:
			return "UDP";
		default:
			return "";
	}
}


//IPv4PacketHeader
IPv4PacketHeader::IPv4PacketHeader(const u_char* pkt_data, struct pcap_pkthdr *header){
	ip_header *ih;
	ih = (ip_header *) (pkt_data + 14); // 14 is the length of Ethernet Header
	version = ih->ver_ihl >> 4;
	ihl = ih->ver_ihl << 4;
	tos = ih->tos;
	//length = ih->tlen;
	length = header->len;
	identification = ih->identification;
	flags = ih->flags_fo >> 3;
	offset = ih->flags_fo << 5;
	ttl = ih->ttl;
	protocol = ih->proto;
	crc = ih->crc;
	source = IPv4Address (ih->saddr.byte1, ih->saddr.byte2, ih->saddr.byte3, ih->saddr.byte4 );
	destination = IPv4Address (ih->daddr.byte1, ih->daddr.byte2, ih->daddr.byte3, ih->daddr.byte4 );
	options = ih->op_pad >> 24;
	padding = ih->op_pad << 8;
}
IPAddress& IPv4PacketHeader::getSourceIPAddress(){
	return source;
}
IPAddress& IPv4PacketHeader::getDestinationIPAddress(){
	return destination;
}
u_char IPv4PacketHeader::getIHL() const{
	return ihl;
}
u_char IPv4PacketHeader::getTypeOfService() const{
	return tos;
}
u_char IPv4PacketHeader::getFlags() const{
	return flags;
}
u_char IPv4PacketHeader::getOffset() const{
	return offset;
}
u_char IPv4PacketHeader::getTimeToLive() const{
	return ttl;
}
u_char IPv4PacketHeader::getPadding() const{
	return padding;
}
u_short IPv4PacketHeader::getIdentification() const{
	return identification;
}
u_short IPv4PacketHeader::getChecksum() const{
	return crc;
}
u_int IPv4PacketHeader::getOptions() const{
	return options;
}



// IPv6PacketHeader
IPv6PacketHeader::IPv6PacketHeader(const u_char* pkt_data, struct pcap_pkthdr* header){
	ip6_header *ih;
	ih = (ip6_header *) (pkt_data + 14); // 14 is the length of Ethernet Header
	version = ih->ver_class_label >> 4;
	traffic_class = ih->ver_class_label >> 12 << 8;
	label = ih->ver_class_label << 20;
	//length = ih->plen;
	length = header->len;
	protocol = ih->proto;
	hop_limit = ih->hop_limit;
	source = IPv6Address (ih->saddr.comp1, ih->saddr.comp2, ih->saddr.comp3, ih->saddr.comp4, 
							ih->saddr.comp5, ih->saddr.comp6, ih->saddr.comp7, ih->saddr.comp8 );
	destination = IPv6Address (ih->daddr.comp1, ih->daddr.comp2, ih->daddr.comp3, ih->daddr.comp4, 
							ih->daddr.comp5, ih->daddr.comp6, ih->daddr.comp7, ih->daddr.comp8 );
}
IPAddress& IPv6PacketHeader::getSourceIPAddress(){
	return source;
}
IPAddress& IPv6PacketHeader::getDestinationIPAddress(){
	return destination;
}
u_char IPv6PacketHeader::getTrafficClass() const{
	return traffic_class;
}
u_char IPv6PacketHeader::getHopLimit() const{
	return hop_limit;
}
u_int IPv6PacketHeader::getLabel() const{
	return label;
}
