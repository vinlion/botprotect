/*
 * flow.cc
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "flow.h"

Flow::Flow(Packet& packet){
	key = FlowKey(packet);
	start = packet.getTime();
	end = start;
	packet_count = 1;
	protocol = "UNCHECKED";
	length = packet.getPacketHeader()->getLength();
	flags_summary = packet.getFlags();
}

Flow::Flow(FlowKey key){
	this->key = key;
}

Flow& Flow::operator += (Packet& packet){
	// Suppose that the flow have the same key with Packet
	if (start > packet.getTime())
		start = packet.getTime();
	if (end < packet.getTime())
		end = packet.getTime();
	length += packet.getPacketHeader()->getLength();
	++packet_count;
	flags_summary &= packet.getFlags(); // Perform a logical & so that at the end, we only get a non null value if the same flag is trigerred all along the conversation
	return *this;
}

FlowKey& Flow::getKey(){
	return key;
}
FlowKey Flow::getKey() const{
	return FlowKey(key);
}
int Flow::getLength() const{
	return length;
}
int Flow::getPacketCount() const {
	return packet_count;
}
std::string Flow::getProtocol(){
	return protocol;
}
timeval Flow::getStart() const{
	return start;
}
timeval Flow::getEnd() const {
	return end;
}

bool Flow::hasExpired(){
	timeval t = start;
	t.tv_sec += TIMEOUT;
	return (t < end);
}

bool Flow::willExpire(timeval time){
	timeval t = start;
	t.tv_sec += TIMEOUT;
	return (t < time);
}
float Flow::duree() const {
	return end - start;
}
float Flow::bandwidth() const{
	/*
	 * First convert in Kilo Bytes before computing the bandwidth. Method should only be called when there is more than one packet but
	 * for safeness, a check is performed
	*/ 
	float dur = duree();
	return dur != 0.0 ? (length/1024.0) / dur : 0.0;
}

int Flow::averagePacketSize(){
	return length / packet_count;
}

// Flags field syntax for TCP packet
/*
 *  0 1 2 3 4 5 6 7
 * +-+-+-+-+-+-+-+-+
 *  C E U A P R S F
 *  W C R C S S Y I
 *  R E G K H T N N
 * +-+-+-+-+-+-+-+-+
*/ 
bool Flow::isRSTFlow() const{
	// Returns true if the flow is composed solely of packets with RST flags
	return flags_summary & 4;
}
bool Flow::isSYNFlow() const{
	// Returns true if the flow is composed solely of packets with SYN flags
	return flags_summary & 2;
}
bool Flow::fromSameSourceAs(Flow& flow){
	// Check if both flows are from the same source IP
	return *(key.getSourceIPAddress()) == *(flow.getKey().getSourceIPAddress());
}
bool Flow::toSameDestinationAs(Flow& flow){
	// Check if both flows are for the same destination IP
	return *(key.getDestinationIPAddress()) == *(flow.getKey().getDestinationIPAddress());
}
bool Flow::operator == (Flow const& other){
	return key == other.getKey();
}
Flow Flow::operator - (){
	return Flow(-getKey());
}
ostream& operator << (ostream& flux, Flow& flow) {
	flux << flow.getKey() << " - " << flow.getStart() << " --> " << flow.getEnd() << " ( " <<  flow.getPacketCount() << " paquet(s) - "<< flow.duree() << " s) { " << flow.getLength() << " bytes }";
	return flux;
}
