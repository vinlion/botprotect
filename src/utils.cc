/*
 * utils.cc
 * 
 * Copyright 2018 Boladji Vinny & Lionel Metongnon <vinny.adjibi@outlook.com> ; <lionel.metongnon@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
 #include "utils.h"

std::ostream& operator << (std::ostream& flux, timeval const& time){
	char timestr[16];
    time_t local_tv_sec;
    struct tm ltime;
    //Get the time from the packet header and format it to human readable format.
    local_tv_sec = time.tv_sec;
    ltime = *localtime(&local_tv_sec);
    strftime( timestr, sizeof timestr, "%H:%M:%S.", &ltime);
	flux << timestr << std::setw(6) << std::setfill('0') << time.tv_usec;
	return flux;
}

bool operator == (timeval const& first, timeval const& second){
	return first.tv_sec==second.tv_sec && first.tv_usec==second.tv_usec;
}
bool operator < (timeval const& first, timeval const& second){
	return (first.tv_sec<second.tv_sec) || (first.tv_sec==second.tv_sec && first.tv_usec<second.tv_usec);
}
bool operator > (timeval const& first, timeval const& second){
	return !(first<second);
}
float operator - (timeval const& end, timeval const& start){
	float diff = end.tv_sec - start.tv_sec, usec = end.tv_usec - start.tv_usec, sample = (start.tv_usec > end.tv_usec) ? start.tv_usec : end.tv_usec;
	// Get the maximum of the usecs into sample to help determine the usec part as decimal value
	while (sample >= 1 || sample <= -1 ){ 	// Get to the longest decimal value
		usec/=10;
		sample/=10;
	}
	diff+=usec;
	return diff;
	
}
